# Asteroid Web
This is the frontend code for the Asteroid Sync app.

## Running the app

```
$ npm install 

$ npm run dev

```

Adjust the `.env` file as needed.
